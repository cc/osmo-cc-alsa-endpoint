
#include <osmocom/core/timer.h>
#include <osmocom/core/select.h>
#include <osmocom/cc/endpoint.h>
#include "../libsample/sample.h"
#include "../libsamplerate/samplerate.h"
#include "../libjitter/jitter.h"

enum call_state {
	CALL_STATE_IDLE = 0,    /* no call */
	CALL_STATE_IN_SETUP,    /* incoming connection */
	CALL_STATE_OUT_SETUP,   /* outgoing connection */
	CALL_STATE_IN_OVERLAP,  /* more informatiopn needed */
	CALL_STATE_OUT_OVERLAP, /* more informatiopn needed */
	CALL_STATE_IN_PROCEEDING,/* call is proceeding */
	CALL_STATE_OUT_PROCEEDING,/* call is proceeding */
	CALL_STATE_IN_ALERTING, /* call is ringing */
	CALL_STATE_OUT_ALERTING,/* call is ringing */
	CALL_STATE_CONNECT,     /* call is connected and transmission is enabled */
	CALL_STATE_IN_DISCONNECT,/* incoming disconnected */
	CALL_STATE_OUT_DISCONNECT,/* outgoing disconnected */
};

struct telephone;

struct call_list;

typedef struct telephone {
	osmo_cc_endpoint_t cc_ep;
	struct call_list *call_list;
	const char *name;

	/* settings */
	int serving_location; /* who we serve when sending causes towards interface */
	int early_audio;
	int tx_delay;

	/* sound */
	int loopback;
	int samplerate;		/* sample rate of headphone interface */
	int buffer_size;
	void *sound;		/* headphone interface */

} telephone_t;

typedef struct call_list {
	struct call_list *next;
        telephone_t *telephone_ep;

	/* alsa states */

	/* osmo-cc states */
	uint32_t cc_callref;
	const char *sdp;
	osmo_cc_session_t *cc_session;
	osmo_cc_session_codec_t *codec;
	int codec_negotiated;

	/* call states */
	enum call_state state;

	/* audio states */
	jitter_t tx_dejitter;
	uint16_t loop_sequence;	 /* sequence + ts for loopback mode */
	uint32_t loop_timestamp;
	samplerate_t srstate;
	sample_t tx_buffer[160]; /* transmit audio buffer */
        int tx_buffer_pos; /* current position in transmit audio buffer */
} call_t;

void telephone_destroy(telephone_t *telephone_ep);
telephone_t *telephone_create(void);
int telephone_init(telephone_t *telephone_ep, const char *name, const char *callerid, uint8_t serving_location, int early_audio, const char *audiodev, int samplerate, int latspl, int tx_delay, int loopback);
void cc_message(osmo_cc_endpoint_t *ep, uint32_t callref, osmo_cc_msg_t *msg);
int ui_init(const char *remote_id, int autoalert, int autoanswer);
int ui_work(telephone_t *telephone_ep, int c);
void alsa_work(telephone_t *telephone_ep);

