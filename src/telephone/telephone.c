/* console handling
 *
 * (C) 2020 by Andreas Eversberg <jolly@eversberg.eu>
 * All Rights Reserved
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <stddef.h>
#include <sys/param.h>
#include "../liblogging/logging.h"
#include "../libsample/sample.h"
#include <osmocom/core/select.h>
#include <osmocom/cc/session.h>
#include <osmocom/cc/rtp.h>
#include <osmocom/cc/g711.h>
#include "../libsound/sound.h"
#include "telephone.h"
#include <osmocom/cc/helper.h>

static struct osmo_cc_helper_audio_codecs codecs[] = {
	{ "PCMA", 8000, 1, g711_encode_alaw, g711_decode_alaw },
	{ "PCMU", 8000, 1, g711_encode_ulaw, g711_decode_ulaw },
	{ NULL, 0, 0, NULL, NULL},
};

static const char *cause_name(int cause)
{
	static char cause_str[16];

	switch (cause) {
	case OSMO_CC_ISDN_CAUSE_NORM_CALL_CLEAR:
		return "hangup";
	case OSMO_CC_ISDN_CAUSE_USER_BUSY:
		return "busy";
	case OSMO_CC_ISDN_CAUSE_USER_ALERTING_NA:
		return "no-answer";
	case OSMO_CC_ISDN_CAUSE_DEST_OOO:
	case OSMO_CC_ISDN_CAUSE_NETWORK_OOO:
		return "out-of-order";
	case OSMO_CC_ISDN_CAUSE_INV_NR_FORMAT:
		return "invalid-number";
	case OSMO_CC_ISDN_CAUSE_NO_CIRCUIT_CHAN:
		return "no-channel";
	case OSMO_CC_ISDN_CAUSE_TEMP_FAILURE:
		return "link-failure";
	case OSMO_CC_ISDN_CAUSE_RESOURCE_UNAVAIL:
		return "resource-unavail";
	default:
		sprintf(cause_str, "cause=%d", cause);
		return cause_str;
	}

}

static char ui_text[256];
static int ui_autoalert;
static int ui_autoanswer;
static int ui_len = 0;
#define UI_MAX_DIGITS 33
static char ui_remote_id[256] = ""; /* what we dial or who called us */
static char ui_remote_dialing[256] = ""; /* what remote called us with */
static char ui_local_id[256] = ""; /* our ID */
static const char ui_digits[] = "0123456789*#ABCD"; /* all digits that can be used */
static uint8_t ui_cause = 0;

static void append_string(char *string, size_t size, char c)
{
	size_t len = strlen(string);
	int i;

	/* string full */
	if (len == size - 1)
		return;

	for (i = 0; i < (int)strlen(ui_digits); i++) {
		if (c == ui_digits[i]) {
			string[len] = c;
			string[len + 1] = '\0';
			break;
		}
	}
}

/*
 * Endpoint instance
 */

/* create interface instance */
telephone_t *telephone_create(void)
{
	telephone_t *telephone_ep;

	telephone_ep = calloc(1, sizeof(*telephone_ep));
	if (!telephone_ep) {
		LOGP(DTEL, LOGL_ERROR, "No memory!\n");
		abort();
	}

	LOGP(DTEL, LOGL_DEBUG, "Telephone instance created\n");

	return telephone_ep;
}

static void telephone_close(telephone_t *telephone_ep);
static void call_destroy(call_t *call);

/* destroy interface instance and free all resource */
void telephone_destroy(telephone_t *telephone_ep)
{
	/* remove stack instance */
	telephone_close(telephone_ep);

	/* destroy all calls */
	while (telephone_ep->call_list)
		call_destroy(telephone_ep->call_list);

	free((char *)telephone_ep->name);

	free(telephone_ep);

	LOGP(DTEL, LOGL_DEBUG, "Telephone instance destroyed\n");
}

/* initialization and configuration of interface instance */
int telephone_init(telephone_t *telephone_ep, const char *name, const char *callerid, uint8_t serving_location, int early_audio, const char *audiodev, int samplerate, int __attribute__((unused)) buffer_size, int tx_delay, int loopback)
{
	telephone_ep->name = strdup(name);
	telephone_ep->serving_location = serving_location;
	telephone_ep->early_audio = early_audio;
	telephone_ep->samplerate = samplerate;
	telephone_ep->buffer_size = buffer_size;
	telephone_ep->tx_delay = tx_delay;
	telephone_ep->loopback = loopback;
	strcpy(ui_local_id, callerid);

	if (audiodev) {
#ifdef HAVE_ALSA
		/* open sound device for call control */
		/* use factor 1.4 of speech level for complete range of sound card */
		telephone_ep->sound = sound_open(SOUND_DIR_DUPLEX, audiodev, NULL, NULL, NULL, 1, 0.0, samplerate, buffer_size, 1.0, 1 / (SPEECH_LEVEL * 0.7079), 4000.0, 2.0);
		if (!telephone_ep->sound) {
			LOGP(DTEL, LOGL_ERROR, "No sound device!\n");
			return -EIO;
		}
		sound_start(telephone_ep->sound);
#else
		LOGP(DTEL, LOGL_ERROR, "No sound card support compiled in!\n");
		return -ENOTSUP;
#endif
	}

	return 0;
}

static void telephone_close(telephone_t __attribute__((unused)) *telephone_ep)
{
#ifdef HAVE_ALSA
	if (telephone_ep->sound) {
		sound_close(telephone_ep->sound);
		telephone_ep->sound = NULL;
	}
#endif
}

/*
 * call instance
 */

static const char *call_state_name[] = {
	"on hook",
	"incoming setup",
	"outgoing setup",
	"incoming overlap",
	"outgoing overlap",
	"incoming proceeding",
	"outgoing proceeding",
	"incoming alerting",
	"outgoing alerting",
	"connected",
	"incoming disconect",
	"outgoing disconect",
};

static void call_new_state(call_t *call, enum call_state state)
{
	LOGP(DTEL, LOGL_DEBUG, "Call state '%s' -> '%s'\n", call_state_name[call->state], call_state_name[state]);
	call->state = state;
}

static call_t *call_create(telephone_t *telephone_ep)
{
	call_t *call, **call_p;
	int rc;

	call = calloc(1, sizeof(*call));
	if (!call) {
		LOGP(DTEL, LOGL_ERROR, "No memory!\n");
		abort();
	}

	call_p = &telephone_ep->call_list;
	while (*call_p)
		call_p = &((*call_p)->next);
	*call_p = call;

	call->telephone_ep = telephone_ep;

	/* init sample rate conversion */ 
	rc = init_samplerate(&call->srstate, 8000.0, (double)telephone_ep->samplerate, 3700.0);
	if (rc < 0)
		abort();

	/* allocate jitter buffer */
	if (telephone_ep->tx_delay)
		rc = jitter_create(&call->tx_dejitter, "tx", 8000, (double)telephone_ep->tx_delay / 1000.0, (double)telephone_ep->tx_delay / 1000.0 * 2.0, JITTER_FLAG_NONE);
	else
		rc = jitter_create(&call->tx_dejitter, "tx", 8000, JITTER_AUDIO);
	if (rc < 0)
		abort();

	LOGP(DTEL, LOGL_DEBUG, "Created new call instance\n");

	return call;
}

static void call_destroy(call_t *call)
{
	call_t **call_p;

	/* free sdp */
	free((char *)call->sdp);

	/* free jitter buffer */
	jitter_destroy(&call->tx_dejitter);

	/* free session description */
	if (call->cc_session)
		osmo_cc_free_session(call->cc_session);

	/* detach */
	call_p = &call->telephone_ep->call_list;
	while (*call_p) {
		if (*call_p == call)
			break;
		call_p = &((*call_p)->next);
	}
	*call_p = call->next;

	free(call);

	LOGP(DTEL, LOGL_DEBUG, "destroyed call instance\n");
}

/*
 * audio handling
 */

/* take audio from CC and store in jitter buffer */
static void down_audio(struct osmo_cc_session_codec *codec, uint8_t marker, uint16_t sequence, uint32_t timestamp, uint32_t ssrc, uint8_t *payload, int payload_len)
{
	call_t *call = codec->media->session->priv;
	jitter_frame_t *jf;

	if (call->telephone_ep->loopback == 3)
		return;

	jf = jitter_frame_alloc(codec->decoder, call, payload, payload_len, marker, sequence, timestamp, ssrc);
	if (!jf)
		return;
	jitter_save(&call->tx_dejitter, jf);
}

void alsa_work(telephone_t *telephone_ep)
{
	if (!telephone_ep->sound)
		return;

#ifdef HAVE_ALSA
	/* handle audio, if sound device is used */
	call_t *call;
	sample_t samples[telephone_ep->buffer_size + 10], *samples_list[1];
	uint8_t *power_list[1];
	int count, input_num;
	int rc;

	/* hunt for call */
	for (call = telephone_ep->call_list; call; call = call->next)
		break; // just any call

	count = sound_get_tosend(telephone_ep->sound, telephone_ep->buffer_size);
	if (count < 0) {
		LOGP(DTEL, LOGL_ERROR, "Failed to get samples in buffer (rc = %d)!\n", count);
		if (count == -EPIPE)
			LOGP(DTEL, LOGL_ERROR, "Trying to recover.\n");
		return;
	}

	if (count > 0) {
		if (call) {
			input_num = samplerate_upsample_input_num(&call->srstate, count);
			int16_t spl[input_num];
			jitter_load_samples(&call->tx_dejitter, (uint8_t *)spl, input_num, sizeof(*spl), jitter_conceal_s16, NULL);
			int16_to_samples_speech(samples, (int16_t *)spl, input_num);
			samplerate_upsample(&call->srstate, samples, input_num, samples, count);
		} else
			memset(samples, 0, sizeof(*samples) * count);
		samples_list[0] = samples;
		power_list[0] = NULL;
		rc = sound_write(telephone_ep->sound, samples_list, power_list, count, NULL, NULL, 1);
		if (rc < 0) {
			LOGP(DTEL, LOGL_ERROR, "Failed to write TX data to sound device (rc = %d)\n", rc);
			if (rc == -EPIPE)
				LOGP(DTEL, LOGL_ERROR, "Trying to recover.\n");
			return;
		}
	}
	samples_list[0] = samples;
	count = sound_read(telephone_ep->sound, samples_list, telephone_ep->buffer_size, 1, NULL);
	if (count < 0) {
		LOGP(DTEL, LOGL_ERROR, "Failed to read from sound device (rc = %d)!\n", count);
		if (count == -EPIPE)
			LOGP(DTEL, LOGL_ERROR, "Trying to recover.\n");
		return;
	}
	if (call && count) {
		int i;

		count = samplerate_downsample(&call->srstate, samples, count);
		/* put samples into ring buffer */
		for (i = 0; i < count; i++) {
			call->tx_buffer[call->tx_buffer_pos] = samples[i];
			/* if ring buffer wrapps, deliver data down to call process */
			if (++call->tx_buffer_pos == 160) {
				call->tx_buffer_pos = 0;
				if (telephone_ep->loopback == 3) {
					jitter_frame_t *jf;
					int16_t spl[160];
					samples_to_int16_speech(spl, call->tx_buffer, 160);
					jf = jitter_frame_alloc(NULL, NULL, (uint8_t *)spl, sizeof(spl), 0, call->loop_sequence, call->loop_timestamp, 12345);
					if (!jf)
						return;
					call->loop_sequence += 1;
					call->loop_timestamp += 160;
					jitter_save(&call->tx_dejitter, jf);
				}
				/* only if we have a call */
				if (call->cc_callref && call->codec) {
					int16_t spl[160];
					uint8_t *payload;
					int payload_len;
					samples_to_int16_speech(spl, call->tx_buffer, 160);
					/* encode and send via RTP */
					call->codec->encoder((uint8_t *)spl, sizeof(spl), &payload, &payload_len, call);
					osmo_cc_rtp_send(call->codec, payload, payload_len, 0, 1, 160);
				}
			}
		}
	}
#endif
}


/*
 * handle message from CC
 */

static void release_reject_ind(call_t *call, uint8_t msg_type, uint8_t isdn_cause)
{
	osmo_cc_msg_t *new_msg;

	/* create osmo-cc message */
	new_msg = osmo_cc_new_msg(msg_type);
	/* cause */
	osmo_cc_add_ie_cause(new_msg, call->telephone_ep->serving_location, isdn_cause, 0, 0);
	/* send message to osmo-cc */
	osmo_cc_ll_msg(&call->telephone_ep->cc_ep, call->cc_callref, new_msg);
	/* change state */
	call_new_state(call, CALL_STATE_IDLE);
	/* destroy call */
	call_destroy(call);
}

void cc_message(osmo_cc_endpoint_t *ep, uint32_t callref, osmo_cc_msg_t *msg)
{
	telephone_t *telephone_ep = ep->priv;
	call_t *call, *other;
	osmo_cc_msg_t *new_msg;
	uint8_t type, plan, present, screen;
	char callerid[128], dialing[128];
	uint8_t coding, location, progress, socket_cause;
	uint16_t sip_cause;
	int rc;
	int i;

	/* hunt for callref */
	call = telephone_ep->call_list;
	while (call) {
		if (call->cc_callref == callref)
			break;
		call = call->next;
	}

	/* process SETUP */
	if (!call) {
		if (msg->type != OSMO_CC_MSG_SETUP_REQ) {
			LOGP(DTEL, LOGL_ERROR, "received message without call instance, please fix!\n");
			goto done;
		}
		/* creating call instance */
		call = call_create(telephone_ep);
		if (!call) {
			LOGP(DTEL, LOGL_ERROR, "Cannot create calll instance.\n");
			abort();
		}
		/* link with cc */
		call->cc_callref = callref;
	}

	switch (msg->type) {
	case OSMO_CC_MSG_SETUP_REQ: /* dial-out command received from epoint */
		/* hunt for call */
		for (other = telephone_ep->call_list; other; other = other->next) {
			if (other != call)
				break;
		}
		if (other) {
			release_reject_ind(call, OSMO_CC_MSG_REJ_IND, OSMO_CC_ISDN_CAUSE_USER_BUSY);
			break;
		}
		/* calling */
		rc = osmo_cc_get_ie_calling(msg, 0, &type, &plan, &present, &screen, callerid, sizeof(callerid));
		if (rc >= 0)
			strncpy(ui_remote_id, callerid, sizeof(ui_remote_id) - 1);
		/* called */
		rc = osmo_cc_get_ie_called(msg, 0, &type, &plan, dialing, sizeof(dialing));
		if (rc >= 0)
			strncpy(ui_remote_dialing, dialing, sizeof(ui_remote_dialing) - 1);
		LOGP(DTEL, LOGL_INFO, "Incoming call from '%s' to '%s'\n", ui_remote_id, ui_remote_dialing);
		/* sdp accept */
		call->sdp = osmo_cc_helper_audio_accept(&ep->session_config, call, codecs, down_audio, msg, &call->cc_session, &call->codec, 0);
		if (!call->sdp) {
			release_reject_ind(call, OSMO_CC_MSG_REJ_IND, OSMO_CC_ISDN_CAUSE_RESOURCE_UNAVAIL);
			break;
		}
		call->sdp = strdup(call->sdp);
		/* change state */
		call_new_state(call, CALL_STATE_IN_SETUP);
		if (ui_autoanswer) {
			/* create osmo-cc message */
			new_msg = osmo_cc_new_msg(OSMO_CC_MSG_SETUP_CNF);
			/* sdp */
			osmo_cc_add_ie_sdp(new_msg, call->sdp);
			free((char *)call->sdp);
			call->sdp = NULL;
			/* send message to osmo-cc */
			osmo_cc_ll_msg(&call->telephone_ep->cc_ep, call->cc_callref, new_msg);
			/* change state */
			call_new_state(call, CALL_STATE_CONNECT);
		} else
		if (ui_autoalert) {
			/* create osmo-cc message */
			new_msg = osmo_cc_new_msg(OSMO_CC_MSG_ALERT_IND);
			if (call->telephone_ep->early_audio) {
				/* progress */
				osmo_cc_add_ie_progress(new_msg, OSMO_CC_CODING_ITU_T, call->telephone_ep->serving_location, OSMO_CC_PROGRESS_INBAND_INFO_AVAILABLE);
				/* sdp */
				osmo_cc_add_ie_sdp(new_msg, call->sdp);
				free((char *)call->sdp);
				call->sdp = NULL;
			}
			/* send message to osmo-cc */
			osmo_cc_ll_msg(&call->telephone_ep->cc_ep, call->cc_callref, new_msg);
			/* change state */
			call_new_state(call, CALL_STATE_IN_ALERTING);
		} else {
			/* create osmo-cc message */
			new_msg = osmo_cc_new_msg(OSMO_CC_MSG_SETUP_ACK_IND);
			if (call->telephone_ep->early_audio) {
				/* progress */
				osmo_cc_add_ie_progress(new_msg, OSMO_CC_CODING_ITU_T, call->telephone_ep->serving_location, OSMO_CC_PROGRESS_INBAND_INFO_AVAILABLE);
				/* sdp */
				osmo_cc_add_ie_sdp(new_msg, call->sdp);
				free((char *)call->sdp);
				call->sdp = NULL;
			}
			/* send message to osmo-cc */
			osmo_cc_ll_msg(&call->telephone_ep->cc_ep, call->cc_callref, new_msg);
			/* change state */
			call_new_state(call, CALL_STATE_IN_OVERLAP);
		}
		break;
	case OSMO_CC_MSG_SETUP_ACK_REQ: /* more information is needed */
		rc = osmo_cc_helper_audio_negotiate(msg, &call->cc_session, &call->codec);
		if (rc < 0) {
			release_reject_ind(call, OSMO_CC_MSG_REL_IND, OSMO_CC_ISDN_CAUSE_RESOURCE_UNAVAIL);
			break;
		}
		LOGP(DTEL, LOGL_INFO, "Incoming call acknowledged\n");
		/* change state */
		call_new_state(call, CALL_STATE_OUT_OVERLAP);
		break;
	case OSMO_CC_MSG_PROC_REQ: /* call of endpoint is proceeding */
		rc = osmo_cc_helper_audio_negotiate(msg, &call->cc_session, &call->codec);
		if (rc < 0) {
			release_reject_ind(call, OSMO_CC_MSG_REL_IND, OSMO_CC_ISDN_CAUSE_RESOURCE_UNAVAIL);
			break;
		}
		LOGP(DTEL, LOGL_INFO, "Incoming call proceeding\n");
		/* change state */
		call_new_state(call, CALL_STATE_OUT_PROCEEDING);
		break;
	case OSMO_CC_MSG_ALERT_REQ: /* call of endpoint is ringing */
		rc = osmo_cc_helper_audio_negotiate(msg, &call->cc_session, &call->codec);
		if (rc < 0) {
			release_reject_ind(call, OSMO_CC_MSG_REL_IND, OSMO_CC_ISDN_CAUSE_RESOURCE_UNAVAIL);
			break;
		}
		LOGP(DTEL, LOGL_INFO, "Incoming call alerting\n");
		/* change state */
		call_new_state(call, CALL_STATE_OUT_ALERTING);
		break;
	case OSMO_CC_MSG_SETUP_RSP: /* call of endpoint is connected */
		rc = osmo_cc_helper_audio_negotiate(msg, &call->cc_session, &call->codec);
		if (rc < 0) {
			release_reject_ind(call, OSMO_CC_MSG_REL_IND, OSMO_CC_ISDN_CAUSE_RESOURCE_UNAVAIL);
			break;
		}
		LOGP(DTEL, LOGL_INFO, "Incoming call acknowledged\n");
		/* create osmo-cc message */
		new_msg = osmo_cc_new_msg(OSMO_CC_MSG_SETUP_COMP_IND);
		/* send message to osmo-cc */
		osmo_cc_ll_msg(&call->telephone_ep->cc_ep, call->cc_callref, new_msg);
		/* change state */
		call_new_state(call, CALL_STATE_CONNECT);
		break;
	case OSMO_CC_MSG_SETUP_COMP_REQ: /* call of endpoint is connected */
		break;
	case OSMO_CC_MSG_INFO_REQ: /* overlap dialing */
		rc = osmo_cc_get_ie_called(msg, 0, &type, &plan, dialing, sizeof(dialing));
		if (rc < 0)
			dialing[0] = '\0';
		LOGP(DTEL, LOGL_INFO, "Incoming call received additional dialing '%s'\n", dialing);
		for (i = 0; dialing[i]; i++) {
			/* add to dial string */
			append_string(ui_remote_dialing, sizeof(ui_remote_dialing), dialing[i]);
		}
		break;
	case OSMO_CC_MSG_PROGRESS_REQ: /* progress */
		rc = osmo_cc_helper_audio_negotiate(msg, &call->cc_session, &call->codec);
		if (rc < 0) {
			release_reject_ind(call, OSMO_CC_MSG_REL_IND, OSMO_CC_ISDN_CAUSE_RESOURCE_UNAVAIL);
			break;
		}
		LOGP(DTEL, LOGL_INFO, "Incoming call received progress\n");
		break;
	case OSMO_CC_MSG_NOTIFY_REQ: /* display and notifications */
		break;
	case OSMO_CC_MSG_REJ_REQ: /* call has been rejected */
		/* get cause */
		rc = osmo_cc_get_ie_cause(msg, 0, &location, &ui_cause, &sip_cause, &socket_cause);
		if (rc < 0)
			ui_cause = OSMO_CC_ISDN_CAUSE_NORM_CALL_CLEAR;
		else
			LOGP(DTEL, LOGL_INFO, "Incoming call rejected: ISDN cause = %d, SIP cause = %d, socket cause = %d\n", ui_cause, sip_cause, socket_cause);
		/* change state */
		call_new_state(call, CALL_STATE_IDLE);
		/* destroy call */
		call_destroy(call);
		break;
	case OSMO_CC_MSG_DISC_REQ: /* call has been disconnected */
		rc = osmo_cc_helper_audio_negotiate(msg, &call->cc_session, &call->codec);
		if (rc < 0) {
			release_reject_ind(call, OSMO_CC_MSG_REL_IND, OSMO_CC_ISDN_CAUSE_RESOURCE_UNAVAIL);
			break;
		}
		/* get cause */
		rc = osmo_cc_get_ie_cause(msg, 0, &location, &ui_cause, &sip_cause, &socket_cause);
		if (rc < 0)
			ui_cause = OSMO_CC_ISDN_CAUSE_NORM_CALL_CLEAR;
		else
			LOGP(DTEL, LOGL_INFO, "Disconnect by remote: ISDN cause = %d, SIP cause = %d, socket cause = %d\n", ui_cause, sip_cause, socket_cause);
		/* change state */
		call_new_state(call, CALL_STATE_IN_DISCONNECT);
		/* progress indicator */
		rc = osmo_cc_get_ie_progress(msg, 0, &coding, &location, &progress);
		if (rc < 0 || coding != OSMO_CC_CODING_ITU_T || !(progress == 1 || progress == 8)) {
			LOGP(DTEL, LOGL_INFO, "no audio after disconnect, releasing!\n");
			release_reject_ind(call, OSMO_CC_MSG_REL_IND, ui_cause);
		}

		break;
	case OSMO_CC_MSG_REL_REQ: /* release call */
		/* get cause */
		rc = osmo_cc_get_ie_cause(msg, 0, &location, &ui_cause, &sip_cause, &socket_cause);
		if (rc < 0)
			ui_cause = OSMO_CC_ISDN_CAUSE_NORM_CALL_CLEAR;
		else
			LOGP(DTEL, LOGL_INFO, "Disconnect by remote: ISDN cause = %d, SIP cause = %d, socket cause = %d\n", ui_cause, sip_cause, socket_cause);
		/* create osmo-cc message */
		new_msg = osmo_cc_new_msg(OSMO_CC_MSG_REL_CNF);
		/* send message to osmo-cc */
		osmo_cc_ll_msg(&call->telephone_ep->cc_ep, call->cc_callref, new_msg);
		/* change state */
		call_new_state(call, CALL_STATE_IDLE);
		/* destroy call */
		call_destroy(call);
		break;
	default:
		LOGP(DTEL, LOGL_ERROR, "received an unsupported CC message: %d\n", msg->type);
	}

done:
	osmo_cc_free_msg(msg);
}

/*
 * user interface
 */


int ui_init(const char *remote_id, int autoalert, int autoanswer)
{
	/* Put scrolling window one line above bottom. */
	logging_limit_scroll_bottom(1);

	ui_autoalert = autoalert;
	ui_autoanswer = autoanswer;

	strncpy(ui_remote_id, remote_id, sizeof(ui_remote_id) - 1);
	ui_remote_id[sizeof(ui_remote_id) - 1] = '\0';

	return 0;
}

int ui_work(telephone_t *telephone_ep, int c)
{
	int work = (c > 0) ? 1 : 0;
	call_t *call;
	osmo_cc_msg_t *msg;
	char text[1024] = "";
	char display[UI_MAX_DIGITS + 1];
	int len, w, h;

	/* hunt for call */
	for (call = telephone_ep->call_list; call; call = call->next)
		break; // just any call
	if (!call) {
		if (c > 0) {
			append_string(ui_remote_id, sizeof(ui_remote_id), c);
			if ((c == 8 || c == 127) && strlen(ui_remote_id))
				ui_remote_id[strlen(ui_remote_id) - 1] = '\0';
dial_after_hangup:
			if (c == 'd') {
				LOGP(DTEL, LOGL_INFO, "Outgoing call from '%s' to '%s'\n", ui_local_id, ui_remote_id);
				ui_remote_dialing[0] = '\0';
				/* creating call instance */
				call = call_create(telephone_ep);
				if (!call) {
					LOGP(DTEL, LOGL_ERROR, "Cannot create calll instance.\n");
					abort();
				}

				/* setup message */
				msg = osmo_cc_new_msg(OSMO_CC_MSG_SETUP_IND);
				/* network type */
				osmo_cc_add_ie_calling_network(msg, OSMO_CC_NETWORK_ALSA_NONE, "");
				/* calling number */
				osmo_cc_add_ie_calling(msg, OSMO_CC_TYPE_UNKNOWN, OSMO_CC_PLAN_TELEPHONY, OSMO_CC_PRESENT_ALLOWED, OSMO_CC_SCREEN_USER_UNSCREENED, ui_local_id);
				/* called number */
				osmo_cc_add_ie_called(msg, OSMO_CC_TYPE_UNKNOWN, OSMO_CC_PLAN_TELEPHONY, ui_remote_id);
				/* sdp offer */
				call->cc_session = osmo_cc_helper_audio_offer(&telephone_ep->cc_ep.session_config, call, codecs, down_audio, msg, 1);
				if (!call->cc_session) {
					LOGP(DTEL, LOGL_NOTICE, "Failed to offer audio, call aborted.\n");
					osmo_cc_free_msg(msg);
					call_destroy(call);
				} else {
					/* create new call */
					osmo_cc_call_t *cc_call = osmo_cc_call_new(&call->telephone_ep->cc_ep);
					call->cc_callref = cc_call->callref;
					/* send message to CC */
					osmo_cc_ll_msg(&call->telephone_ep->cc_ep, call->cc_callref, msg);
					/* change state */
					call_new_state(call, CALL_STATE_OUT_SETUP);
				}
			}
		}
		memset(display, '.', UI_MAX_DIGITS);
		memcpy(display, ui_remote_id, strlen(ui_remote_id));
		display[UI_MAX_DIGITS] = '\0';
		sprintf(text, "%s: %s (press digits %s or d=dial)", call_state_name[(call) ? call->state : CALL_STATE_IDLE], display, ui_digits);
		goto done;
	}

	if (c == 'h' || (c == 'd' && call->state == CALL_STATE_IN_DISCONNECT)) {
		LOGP(DTEL, LOGL_INFO, "Call hangup\n");
		/* create osmo-cc message */
		if (call->state == CALL_STATE_IN_SETUP)
			release_reject_ind(call, OSMO_CC_MSG_REJ_IND, OSMO_CC_ISDN_CAUSE_NORM_CALL_CLEAR);
		else
			release_reject_ind(call, OSMO_CC_MSG_REL_IND, OSMO_CC_ISDN_CAUSE_NORM_CALL_CLEAR);
		/* dial new number */
		if (c == 'd') {
			call = NULL;
			goto dial_after_hangup;
		}
		goto done;
	}
	if (c == 'o' && call->state == CALL_STATE_IN_SETUP) {
		LOGP(DTEL, LOGL_INFO, "Acknowledge incoming call\n");
		/* create osmo-cc message */
		msg = osmo_cc_new_msg(OSMO_CC_MSG_SETUP_ACK_IND);
		if (call->telephone_ep->early_audio && call->sdp) {
			/* progress */
			osmo_cc_add_ie_progress(msg, OSMO_CC_CODING_ITU_T, call->telephone_ep->serving_location, OSMO_CC_PROGRESS_INBAND_INFO_AVAILABLE);
			/* sdp */
			osmo_cc_add_ie_sdp(msg, call->sdp);
			free((char *)call->sdp);
			call->sdp = NULL;
		}
		/* send message to osmo-cc */
		osmo_cc_ll_msg(&call->telephone_ep->cc_ep, call->cc_callref, msg);
		/* change state */
		call_new_state(call, CALL_STATE_IN_OVERLAP);
	}
	if (c == 'p' && (call->state == CALL_STATE_IN_SETUP || call->state == CALL_STATE_IN_OVERLAP)) {
		LOGP(DTEL, LOGL_INFO, "Proceeding incoming call\n");
		/* create osmo-cc message */
		msg = osmo_cc_new_msg(OSMO_CC_MSG_PROC_IND);
		if (call->telephone_ep->early_audio && call->sdp) {
			/* progress */
			osmo_cc_add_ie_progress(msg, OSMO_CC_CODING_ITU_T, call->telephone_ep->serving_location, OSMO_CC_PROGRESS_INBAND_INFO_AVAILABLE);
			/* sdp */
			osmo_cc_add_ie_sdp(msg, call->sdp);
			free((char *)call->sdp);
			call->sdp = NULL;
		}
		/* send message to osmo-cc */
		osmo_cc_ll_msg(&call->telephone_ep->cc_ep, call->cc_callref, msg);
		/* change state */
		call_new_state(call, CALL_STATE_IN_PROCEEDING);
	}
	if (c == 'a' && (call->state == CALL_STATE_IN_SETUP || call->state == CALL_STATE_IN_OVERLAP || call->state == CALL_STATE_IN_PROCEEDING)) {
		LOGP(DTEL, LOGL_INFO, "Alerting incoming call\n");
		/* create osmo-cc message */
		msg = osmo_cc_new_msg(OSMO_CC_MSG_ALERT_IND);
		if (call->telephone_ep->early_audio && call->sdp) {
			/* progress */
			osmo_cc_add_ie_progress(msg, OSMO_CC_CODING_ITU_T, call->telephone_ep->serving_location, OSMO_CC_PROGRESS_INBAND_INFO_AVAILABLE);
			/* sdp */
			osmo_cc_add_ie_sdp(msg, call->sdp);
			free((char *)call->sdp);
			call->sdp = NULL;
		}
		/* send message to osmo-cc */
		osmo_cc_ll_msg(&call->telephone_ep->cc_ep, call->cc_callref, msg);
		/* change state */
		call_new_state(call, CALL_STATE_IN_ALERTING);
	}
	if (c == 'c' && (call->state == CALL_STATE_IN_SETUP || call->state == CALL_STATE_IN_OVERLAP || call->state == CALL_STATE_IN_PROCEEDING || call->state == CALL_STATE_IN_ALERTING)) {
		LOGP(DTEL, LOGL_INFO, "Answer incoming call\n");
		/* create osmo-cc message */
		msg = osmo_cc_new_msg(OSMO_CC_MSG_SETUP_CNF);
		/* sdp */
		if (call->sdp) {
			osmo_cc_add_ie_sdp(msg, call->sdp);
			free((char *)call->sdp);
			call->sdp = NULL;
		}
		/* send message to osmo-cc */
		osmo_cc_ll_msg(&call->telephone_ep->cc_ep, call->cc_callref, msg);
		/* change state */
		call_new_state(call, CALL_STATE_CONNECT);
	}
	if (c > 0 && call->state == CALL_STATE_OUT_OVERLAP && strchr(ui_digits, c)) {
		LOGP(DTEL, LOGL_INFO, "Send digit %c\n", c);
		char called[] = { c, '\0' };
		/* create osmo-cc message */
		msg = osmo_cc_new_msg(OSMO_CC_MSG_INFO_IND);
		/* add dialing */
		osmo_cc_add_ie_called(msg, OSMO_CC_TYPE_UNKNOWN, OSMO_CC_PLAN_TELEPHONY, called);
		/* send message to osmo-cc */
		osmo_cc_ll_msg(&call->telephone_ep->cc_ep, call->cc_callref, msg);
		/* add to dial string */
		append_string(ui_remote_id, sizeof(ui_remote_id), c);
	}

	/* NOTE: the state is from console view: a call towards CC is OUT and from CC is IN */
	switch (call->state) {
	case CALL_STATE_OUT_SETUP:
	case CALL_STATE_OUT_PROCEEDING:
	case CALL_STATE_OUT_ALERTING:
		sprintf(text, "%s: %s (press h=hangup)", call_state_name[call->state], ui_remote_id);
		break;
	case CALL_STATE_OUT_OVERLAP:
		sprintf(text, "%s: %s (press digits %s h=hangup)", call_state_name[call->state], ui_remote_id, ui_digits);
		break;
	case CALL_STATE_CONNECT:
		if (ui_remote_dialing[0])
			sprintf(text, "%s: %s->%s (press h=hangup)", call_state_name[call->state], ui_remote_id, ui_remote_dialing);
		else
			sprintf(text, "%s: %s (press h=hangup)", call_state_name[call->state], ui_remote_id);
		break;
	case CALL_STATE_IN_DISCONNECT:
		sprintf(text, "%s: %s (press h=hangup d=redial)", call_state_name[call->state], cause_name(ui_cause));
		break;
	case CALL_STATE_IN_SETUP:
		sprintf(text, "%s: %s->%s (press o=overlap p=proceeding a=alerting c=connect h=hangup)", call_state_name[call->state], ui_remote_id, ui_remote_dialing);
		break;
	case CALL_STATE_IN_OVERLAP:
		sprintf(text, "%s: %s->%s (press p=proceeding a=alerting c=connect h=hangup)", call_state_name[call->state], ui_remote_id, ui_remote_dialing);
		break;
	case CALL_STATE_IN_PROCEEDING:
		sprintf(text, "%s: %s->%s (press a=alerting c=connect h=hangup)", call_state_name[call->state], ui_remote_id, ui_remote_dialing);
		break;
	case CALL_STATE_IN_ALERTING:
		sprintf(text, "%s: %s->%s (press c=connect h=hangup)", call_state_name[call->state], ui_remote_id, ui_remote_dialing);
		break;
	default:
		break;
	}

done:
	/* skip if nothing has changed */
	len = strlen(text);
        if (ui_len == len && !memcmp(ui_text, text, len))
                return work;
        /* lock logging */
        lock_logging();
        /* disable window */
        enable_limit_scroll(false);
        /* geht height */
        get_win_size(&w, &h);
        /* save cursor go to bottom, use white color */
        printf("\0337\033[%d;1H\033[1;37m", h);
        /* copy text and pad with spaces */
        ui_len = len;
        memcpy(ui_text, text, ui_len);
        if (ui_len < (int)MIN(sizeof(ui_text), w))
                memset(ui_text + ui_len, ' ', MIN(sizeof(ui_text), w) - ui_len);
        /* write text */
        fwrite(ui_text, MIN(sizeof(ui_text), w), 1, stdout);
        /* reset color, go back to previous line, flush */
        printf("\033[0;39m\0338");
        /* flush output */
        fflush(stdout);
        /* enable window */
        enable_limit_scroll(true);
        /* unlock logging */
        unlock_logging();

	return work;
}

